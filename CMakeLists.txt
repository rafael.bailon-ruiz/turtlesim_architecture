cmake_minimum_required(VERSION 3.5)
project(turtlesim_architecture)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()
# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

if(DEFINED ENV{ROS_VERSION})
  message(STATUS "Found ROS version: $ENV{ROS_VERSION}")
else()
  message(ERROR "ROS version unkown!")
endif()

if($ENV{ROS_VERSION} EQUAL 1)
  message(STATUS "CMake compiling package for ROS $ENV{ROS_DISTRO}")

  ## Find catkin macros and libraries
  ## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
  ## is used, also find other catkin packages
  find_package(catkin REQUIRED COMPONENTS
    roscpp
    geometry_msgs
    turtlesim
  )

  catkin_package(
    #  INCLUDE_DIRS include
    #  LIBRARIES robot_skills_introspection
    CATKIN_DEPENDS geometry_msgs roscpp turtlesim
    #  DEPENDS system_lib
  )

  include_directories(${catkin_INCLUDE_DIRS})
  add_executable(turtlesim src/turtlesim_ros.cpp)
  target_link_libraries(turtlesim ${catkin_LIBRARIES})

else()
  message(STATUS "CMake compiling package for ROS$ENV{ROS_VERSION} $ENV{ROS_DISTRO}")

  find_package(ament_cmake REQUIRED)
  find_package(ament_cmake_python REQUIRED)
  find_package(std_msgs REQUIRED)

  install(
    PROGRAMS 
      ros2_scripts/turtlesim_controller.py
      ros2_scripts/turtlesim_simulator.py
      ros2_scripts/turtlesim_manual_control.py
      ros2_scripts/turtlesim_cmd_relay.py
    PERMISSIONS 
      WORLD_EXECUTE GROUP_EXECUTE OWNER_EXECUTE
      OWNER_READ GROUP_READ WORLD_READ
    DESTINATION lib/${PROJECT_NAME}
  )

  install(
    FILES 
      ros2_launch/turtlesim_architecture.launch.py
    DESTINATION share/${PROJECT_NAME}/launch
  )

  # Export package dependencies
  ament_export_dependencies(ament_cmake)

  ament_package()

endif()
