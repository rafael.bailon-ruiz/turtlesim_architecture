#!/usr/bin/env python
import rospy
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from math import pow, atan2, sqrt


class turtlebot():

    def __init__(self):
        #Creating our node,publisher and subscriber
        rospy.init_node('turtlebot_controller', anonymous=True)
        self.velocity_publisher = rospy.Publisher(
            '/turtle1/cmd_vel', Twist, queue_size=10)
        self.pose_subscriber = rospy.Subscriber(
            '/turtle1/pose', Pose, self.callback)
        self.goal_subscriber = rospy.Subscriber(
            '/turtle1/goal', Pose, self.goal_callback)
        self.reset_subscriber = rospy.Subscriber(
            '/turtle1/reset_goto', Empty, self.reset_callback)
        self.pose = Pose()
        self.goal = None
        self.rate = rospy.Rate(10)
        self.distance_tolerance = .01

    #Callback function implementing the pose value received
    def callback(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 4)
        self.pose.y = round(self.pose.y, 4)

    def reset_callback(self, msg):
        rospy.loginfo("Reset controller")
        self.goal = None

    def goal_callback(self, data):
        self.goal = data
        rospy.loginfo("Received goal %d, %d", self.goal.x, self.goal.y)

    def euclidean_distance(self):
        """Euclidean distance between current pose and the goal."""
        return sqrt(pow((self.goal.x - self.pose.x), 2) +
                    pow((self.goal.y - self.pose.y), 2))

    def linear_vel(self, constant=.5):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return constant * self.euclidean_distance()

    def steering_angle(self):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return atan2(self.goal.y - self.pose.y, self.goal.x - self.pose.x)

    def angular_vel(self, constant=5):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return constant * (self.steering_angle() - self.pose.theta)

    def move2goal(self):
        vel_msg = Twist()

        while not rospy.is_shutdown():
            if self.goal is None:
                pass

            elif self.euclidean_distance() >= self.distance_tolerance:
                #Porportional Controller
                #angular velocity in the z-axis:
                vel_msg.angular.x = 0
                vel_msg.angular.y = 0
                vel_msg.angular.z = self.angular_vel()

                if vel_msg.angular.z < .1:
                    #linear velocity in the x-axis:
                    vel_msg.linear.x = self.linear_vel()
                else:
                    vel_msg.linear.x = 0
                vel_msg.linear.y = 0
                vel_msg.linear.z = 0

                rospy.logdebug("publish speed %s", vel_msg)
                #Publishing our vel_msg
                self.velocity_publisher.publish(vel_msg)

            else:
                #Stopping our robot after the movement is over
                vel_msg.linear.x = 0
                vel_msg.angular.z = 0
                rospy.logdebug("publish speed %s", vel_msg)
                self.velocity_publisher.publish(vel_msg)
                self.goal = None

            self.rate.sleep()


if __name__ == '__main__':
    try:
        #Testing our function
        x = turtlebot()
        x.move2goal()

    except rospy.ROSInterruptException:
        pass
