#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist

class relay(Node):

    def __init__(self):
        Node.__init__(self, 'cmd_relay')
        self.publisher = self.create_publisher(Twist, '/turtle1/cmd_vel', 10)
        self.subscriber = self.create_subscription(Twist, '/turtle1/manual_vel', self.callback, 10)

    def callback(self, data):
        self.publisher.publish(data)

if __name__ == '__main__':
    rclpy.init()
    x = relay()
    rclpy.spin(x)
