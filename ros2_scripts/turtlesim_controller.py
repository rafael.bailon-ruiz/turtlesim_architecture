#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from std_msgs.msg import Empty
from math import pow, atan2, sqrt

class turtlebot(Node):

    def __init__(self):
        #Creating our node,publisher and subscriber
        Node.__init__(self, 'turtlebot_controller')
        self.velocity_publisher = self.create_publisher(Twist, '/turtle1/cmd_vel', 10)
        self.pose_subscriber = self.create_subscription(Pose, '/turtle1/pose', self.callback, 10)
        self.goal_subscriber = self.create_subscription(Pose, '/turtle1/goal', self.goal_callback, 1)
        self.reset_subscriber = self.create_subscription(Empty, '/turtle1/reset_goto', self.reset_callback, 1)
        self.pose = Pose()
        self.goal = None
        self.distance_tolerance = .01
        self.rate = self.create_timer(.1, self.move2goal)

    #Callback function implementing the pose value received
    def callback(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 4)
        self.pose.y = round(self.pose.y, 4)

    def goal_callback(self, data):
        self.goal = data
        self.get_logger().info(f"Received goal {self.goal.x}, {self.goal.y}")

    def reset_callback(self, data):
        self.goal = None
        self.get_logger().info(f"Reset controller")

    def euclidean_distance(self):
        """Euclidean distance between current pose and the goal."""
        return sqrt(pow((self.goal.x - self.pose.x), 2) +
                    pow((self.goal.y - self.pose.y), 2))

    def linear_vel(self, constant=.5):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return constant * self.euclidean_distance()

    def steering_angle(self):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return atan2(self.goal.y - self.pose.y, self.goal.x - self.pose.x)

    def angular_vel(self, constant=5):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return constant * (self.steering_angle() - self.pose.theta)

    def move2goal(self):
        vel_msg = Twist()

        if self.goal is None:
            return

        elif self.euclidean_distance() >= self.distance_tolerance:
            #Porportional Controller
            #angular velocity in the z-axis:
            vel_msg.angular.x = 0.0
            vel_msg.angular.y = 0.0
            vel_msg.angular.z = self.angular_vel()

            if vel_msg.angular.z < .1:
                #linear velocity in the x-axis:
                vel_msg.linear.x = self.linear_vel()
            else:
                vel_msg.linear.x = 0.0
            vel_msg.linear.y = 0.0
            vel_msg.linear.z = 0.0

        else:
            #Stopping our robot after the movement is over
            vel_msg.linear.x = 0.0
            vel_msg.angular.z = 0.0
            self.goal = None

        self.get_logger().debug(f"publish speed {vel_msg}")
        self.velocity_publisher.publish(vel_msg)

if __name__ == '__main__':
    rclpy.init()
    #Testing our function
    x = turtlebot()
    rclpy.spin(x)
