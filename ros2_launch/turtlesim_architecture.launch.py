import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    return launch.LaunchDescription([
        launch_ros.actions.Node(
            package='turtlesim_architecture', node_executable='turtlesim_simulator.py', output='screen',
            node_name="turtlesim_simulator_manager"),
        launch_ros.actions.Node(
            package='turtlesim_architecture', node_executable='turtlesim_manual_control.py', output='screen',
            node_name="turtlesim_manual_monitor"),
        launch_ros.actions.Node(
            package='turtlesim_architecture', node_executable='turtlesim_cmd_relay.py', output='screen',
            node_name="turtlesim_cmd_relay"),
        launch_ros.actions.Node(
            package='turtlesim_architecture', node_executable='turtlesim_controller.py', output='screen',
            node_name='turtlesim_controller'),
    ])
